﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpdateWizard.Interface
{
    public interface IRunOnce
    {
        string Remark { get; }

        bool isEnabled { get; }

        string Run(string baseDir);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateWizard.Entities;
using UpdateWizard.Utilities;

namespace UpdateWizard
{
    public class UpdateStrategy
    {
        public static List<UP_FileItem> ComputeUpdateFiles(List<UP_FileItem> totalFileItems)
        {
            List<UP_FileItem> resultFiles = new List<UP_FileItem>();
            var dbcontext = DBFactory.UpdateDBContext;
            int maxVer = dbcontext.Queryable<Entities.UP_Version>().Max(it => it.PublishID);
            int minVer = dbcontext.Queryable<Entities.UP_Version>().Min(it => it.PublishID);
            //for (int index = minVer;index <= maxVer;index++)
            //{
            //    foreach(var fileItem in totalFileItems.Where(it => it.MainVersionID == index).ToList())
            //    {
                    
            //        if(!resultFiles.Any(it => $"{it.RelativePath}{it.FileName}{it.Extension}" == $"{fileItem.RelativePath}{it.FileName}{it.Extension}"))
            //        {
            //            //新增文件则加入升级集合
            //            resultFiles.Add(fileItem);
            //        }
            //        else
            //        {
            //            //同名文件的多次更新，高版本覆盖低版本
            //            var removeItem = resultFiles.Single(it => it.RelativePath == fileItem.RelativePath
            //                                                  && it.FileName == fileItem.FileName
            //                                                  && it.Extension == fileItem.Extension);
            //            resultFiles.Remove(removeItem);
            //            resultFiles.Add(fileItem);
            //        }
            //    }
            //}
            return resultFiles;
        }

        /// <summary>
        /// 查询全局文件摘要
        /// </summary>
        /// <returns></returns>
        public static List<UP_VersionSnapShot> QueryUpdateSummary()
        {
            var result = new List<UP_VersionSnapShot>();
            var fileItems = DBFactory.UpdateDBContext.Queryable<UP_VersionSnapShot>().ToList();
            return fileItems;
        }
    }
}

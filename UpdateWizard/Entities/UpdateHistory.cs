﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    [Serializable]
    public class UpdateHistory
    {
        public int CurrentVersion { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public string UpdateSystem { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    [Serializable]
    public class Wizard
    {
        public string Conn { get; set; }

        public string TraceDirectory { get; set; }

        public string UpdateSystemName { get; set; }

        public string ExtensionFilter { get; set; }

        public List<string> FileNameFilter { get; set; }

        public List<string> DeleteItems { get; set; }


    }
}

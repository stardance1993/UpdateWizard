﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    [SugarTable("UP_UpdateDomain")]
    public class UP_UpdateDomain
    {
        public string  ID { get; set; }

        public string SystemName { get; set; }

        public string Remark { get; set; }

        public short Status { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime ModifyTime { get; set; }

    }
}

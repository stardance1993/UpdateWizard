﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    [SugarTable("UP_Version")]
    public class UP_Version
    {
        public string ID { get; set; }

        public int PublishID { get; set; }

        public string UpdateDomainID { get; set; }

        public string MainExeName { get; set; }

        public string MainExeVersion { get; set; }

        public string Remark { get; set; }

        public DateTime PublishDate { get; set; }

        public short Status { get; set; }


    }
}

﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    [SugarTable("UP_FileItem")]
    public class UP_FileItem
    {
        public string ID { get; set; }

        public string VersionID { get; set; }

        public string RelativePath { get; set; }

        public string FileName { get; set; }

        public string Extension { get; set; }

        public DateTime ModifyTime { get; set; }

        public short ModifyType { get; set; }

        public string MD { get; set; }


    }


    public enum ModifyTypeEnum
    { 
        /// <summary>
        /// 无改动
        /// </summary>
        NOM,
        /// <summary>
        /// 新增
        /// </summary>
        ADD,
        /// <summary>
        /// 修改
        /// </summary>
        MDF,
        /// <summary>
        /// 删除
        /// </summary>
        DEL
    }
}

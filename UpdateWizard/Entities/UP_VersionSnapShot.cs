﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Entities
{
    public class UP_VersionSnapShot
    {
        public string ID { get; set; }

        public string RelativePath { get; set; }

        public string FileName { get; set; }

        public string Extension { get; set; }

        public DateTime ModifyTime { get; set; }

        public string MD { get; set; }

        public short ModifyType { get; set; }


    }
}

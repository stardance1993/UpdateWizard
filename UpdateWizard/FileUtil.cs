﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard
{
    public class FileUtil
    {
        public static void FindAllFiles(string directoryName,ref List<FileInfo> files)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directoryName);
            FileInfo[] currentDirFiles = dirInfo.GetFiles();
            files.AddRange(currentDirFiles.ToList());

            DirectoryInfo[] subDirs = dirInfo.GetDirectories();
            foreach (var subDir in subDirs)
            {
                FindAllFiles(subDir.FullName,ref files);
            }
        }
    }
}

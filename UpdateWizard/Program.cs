﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UpdateWizard.Actions;
using UpdateWizard.Interface;
using UpdateWizard.Utilities;

namespace UpdateWizard
{
    public class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Console.WriteLine("UpdateWizard V0.1");
            while (true)
            {
                Console.Write("$ ");
                string command = Console.ReadLine();
                if (command.Contains("publish"))
                {
                    Actions.Publish.Execute();
                }
                else if (command.Contains("generate"))
                {
                    Actions.Generate.Execute();
                }
                else if (command.Contains("snapshot"))
                {
                    Actions.SnapShot.Execute();
                }
                Console.WriteLine("");
            }
            //Generate.Execute();
            //Publish.Execute();
            //SnapShot.Execute();

        }
    }
}

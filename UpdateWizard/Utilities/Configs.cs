﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateWizard.Entities;

namespace UpdateWizard.Utilities
{
    public class Configs
    {
        public static Wizard BaseConfig
        {
            get
            {
                string config = File.ReadAllText("wizard.json");
                return JsonConvert.DeserializeObject<Wizard>(config);
            }
        }
    }
}

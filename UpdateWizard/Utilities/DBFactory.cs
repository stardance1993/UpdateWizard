﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateWizard.Utilities
{
    public class DBFactory
    {
        public static SqlSugarClient UpdateDBContext
        { 
            get
            {
                return new SqlSugarClient(new ConnectionConfig
                { 
                    ConnectionString = Configs.BaseConfig.Conn,
                    IsAutoCloseConnection = true,
                    DbType = DbType.SqlServer
                });
            }
        }
    }
}

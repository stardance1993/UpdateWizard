﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateWizard.Entities;
using UpdateWizard.Utilities;

namespace UpdateWizard.Actions
{
    public class SnapShot
    {
        //从原始版本遍历到最新版本，对比生成最新的程序文件摘要
        public static bool Execute()
        {
            var result = new List<UP_FileItem>();
            string domain = Configs.BaseConfig.UpdateSystemName;
            var domainEntity = DBFactory.UpdateDBContext.Queryable<UP_UpdateDomain>().Single(it => it.SystemName == domain);
            var versions = DBFactory.UpdateDBContext.Queryable<UP_Version>().Where(it => it.UpdateDomainID == domainEntity.ID).ToList();
            var versionIDs = versions.Select(it => it.ID).ToList();
            var fileItems = DBFactory.UpdateDBContext.Queryable<UP_FileItem>().In(it => it.VersionID, versionIDs).ToList();
            var maxVer = versions.Max(it => it.PublishID);
            var minVer = versions.Min(it => it.PublishID);
            for (int index = minVer; index <= maxVer; index++)
            {
                var currVerId = versions.Single(it => it.PublishID == index).ID;
                var currentVerFiles = fileItems.Where(it => it.VersionID == currVerId).ToList();
                foreach(var file in currentVerFiles)
                {
                    //var condition = new Func<UP_FileItem, bool>(it => $"{it.RelativePath}{it.FileName}{it.Extension}" != $"{file.RelativePath}{file.FileName}{file.Extension}");
                    //新增，则加入
                    if (!result.Any(it => $"{it.RelativePath}{it.FileName}{it.Extension}" == $"{file.RelativePath}{file.FileName}{file.Extension}"))
                    {
                        file.ModifyType = (int)ModifyTypeEnum.ADD;
                        result.Add(file);
                    }
                    else
                    {
                        //删除
                        if (file.ModifyType == (int)ModifyTypeEnum.DEL)
                        {
                            file.ModifyType = (int)ModifyTypeEnum.DEL;
                            result.Add(file);
                        }
                        //修改
                        else  
                        {
                            result.Remove(result.Single(it => $"{it.RelativePath}{it.FileName}{it.Extension}" != $"{file.RelativePath}{file.FileName}{file.Extension}"));
                            file.ModifyType = (int)ModifyTypeEnum.MDF;
                            result.Add(file);
                        }
                       
                    }
                }
            }
            DBFactory.UpdateDBContext.Deleteable<UP_VersionSnapShot>().ExecuteCommand();
            result.ForEach(it =>
            {
                DBFactory.UpdateDBContext.Insertable<UP_VersionSnapShot>(new UP_VersionSnapShot
                {
                    ID = Guid.NewGuid().ToString(),
                    FileName = it.FileName,
                    RelativePath = it.RelativePath,
                    Extension = it.Extension,
                    MD = it.MD,
                    ModifyTime = DateTime.Now
                }).ExecuteCommand();
            }
            );
            return true;
        }
    }
}

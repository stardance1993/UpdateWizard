﻿using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateWizard.Entities;
using UpdateWizard.Utilities;

namespace UpdateWizard.Actions
{
    public class Publish
    {
        public static bool Execute()
        {
            string pathOfGen = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "versionPatch.json");
            string patchContent = File.ReadAllText(pathOfGen);
            List<UP_FileItem> items = JsonConvert.DeserializeObject<List<UP_FileItem>>(patchContent);

            //check update domian
            string updatedomain = Configs.BaseConfig.UpdateSystemName;
            var domainContext = DBFactory.UpdateDBContext.Queryable<UP_UpdateDomain>()
                                                         .First(it => it.SystemName == updatedomain);

            var lastedversion = DBFactory.UpdateDBContext.Queryable<Entities.UP_Version>()
                                                         .Max(it => it.PublishID);
            var newVer = new Entities.UP_Version
            {
                ID = Guid.NewGuid().ToString(),
                MainExeName = "",
                MainExeVersion = "",
                PublishDate = DateTime.Now,
                PublishID = lastedversion + 1,
                Remark = string.Empty,
                Status = 1,
                UpdateDomainID = domainContext.ID
            };
            DBFactory.UpdateDBContext.Insertable<Entities.UP_Version>(newVer).ExecuteCommand();

            items.ForEach(it =>
            {
                it.VersionID = newVer.ID;
                DBFactory.UpdateDBContext.Insertable<Entities.UP_FileItem>(it).ExecuteCommand();
            });

            //生成压缩包
            int i = 0;
            string sourceDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Source");
            if (!Directory.Exists(sourceDir)) Directory.CreateDirectory(sourceDir);
            Directory.Delete(sourceDir, true);
            foreach(var file in items)
            {
                string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Source", file.RelativePath);
                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                string srcPath = Path.Combine(Configs.BaseConfig.TraceDirectory,file.RelativePath,$"{file.FileName}{file.Extension}");
                string targetPath = Path.Combine(sourceDir, file.RelativePath, $"{file.FileName}{file.Extension}");
                File.Copy(srcPath, targetPath);
            }
            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(sourceDir);
                string dirPublishedZip = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"Publish");
                if (!Directory.Exists(dirPublishedZip)) Directory.CreateDirectory(dirPublishedZip);
                string targetFilePath = Path.Combine(dirPublishedZip, $"1.zip");
                zip.Save(targetFilePath);
            }

            Actions.SnapShot.Execute();
            return true;
        }
    }
}

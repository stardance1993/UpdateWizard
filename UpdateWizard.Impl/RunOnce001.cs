﻿using Leviathan.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UpdateWizard.Interface;

namespace UpdateWizard.Impl
{
    public class RunOnce001 : IRunOnce
    {
        public string Remark { get => "修改ini\\Examine\\IsExamine为1"; }
        public bool isEnabled { get =>  true;}

        public string Run(string baseDir)
        {
            Debug.WriteLine("Run方法被调用！");
            return "1";
           
        }


    }
}

/****** Object:  Table [dbo].[UP_VersionSnapShot]    Script Date: 11/10/2019 15:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UP_VersionSnapShot](
	[ID] [nvarchar](64) NOT NULL,
	[RelativePath] [nvarchar](512) NULL,
	[FileName] [nvarchar](128) NULL,
	[Extension] [nvarchar](32) NULL,
	[ModifyTime] [datetime] NULL,
	[MD] [nvarchar](64) NULL,
	[ModifyType] [smallint] NULL
 CONSTRAINT [PK_UP_VersionSnapShot] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'消息摘要' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UP_VersionSnapShot', @level2type=N'COLUMN',@level2name=N'MD'
GO
/****** Object:  Table [dbo].[UP_Version]    Script Date: 11/10/2019 15:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UP_Version](
	[ID] [nvarchar](64) NOT NULL,
	[PublishID] [smallint] NULL,
	[UpdateDomainID] [nvarchar](64) NULL,
	[MainExeName] [nvarchar](64) NULL,
	[MainExeVersion] [nvarchar](32) NULL,
	[Remark] [nvarchar](256) NULL,
	[PublishDate] [datetime] NULL,
	[Status] [smallint] NULL,
 CONSTRAINT [PK_UP_Version] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UP_UpdateDomain]    Script Date: 11/10/2019 15:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UP_UpdateDomain](
	[ID] [nvarchar](64) NOT NULL,
	[SystemName] [nvarchar](64) NULL,
	[Remark] [nvarchar](256) NULL,
	[Status] [smallint] NULL,
	[CreateTime] [datetime] NULL,
	[ModifyTime] [datetime] NULL,
 CONSTRAINT [PK_UP_UpdateDomain] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UP_FileItem]    Script Date: 11/10/2019 15:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UP_FileItem](
	[ID] [nvarchar](64) NOT NULL,
	[VersionID] [nvarchar](64) NULL,
	[RelativePath] [nvarchar](256) NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Extension] [nvarchar](32) NULL,
	[ModifyTime] [datetime] NULL,
	[ModifyType] [smallint] NULL,
	[MD] [nvarchar](64) NULL,
 CONSTRAINT [PK_UP_FileItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
